package com.example.springbootmongodb.Controller;

import com.example.springbootmongodb.Model.SpecificDevice;
import com.example.springbootmongodb.Service.DeviceService;
import com.example.springbootmongodb.Model.Device;
import com.example.springbootmongodb.Service.SpecificDeviceService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/devices")
@AllArgsConstructor

public class DeviceController {
    private  final DeviceService deviceService;
    private  final SpecificDeviceService specificDeviceService;
    //@GetMapping("/phone")
    //public List<Device> fetchAllDevice(){
        //return deviceService.getAllDevices();
    //}
    @CrossOrigin("http://localhost:3000")
    @GetMapping("/phone/search")
    public List<Device> searchDevice(@RequestParam String name,@RequestParam String color,@RequestParam String ram,@RequestParam String rom){
        return deviceService.searchDevice(name,color,ram,rom);
    }
    @CrossOrigin("http://localhost:3000")
    @GetMapping("/pagination")
    public Page<Device> getDevicesWithSort(@RequestParam String name,@RequestParam String color,@RequestParam String ram,@RequestParam String rom,@RequestParam int offset,@RequestParam int pageSize){
        return  deviceService.findProductsWithPagination(name,color,ram,rom,offset,pageSize);
    }
    @CrossOrigin("http://localhost:3000")
    @PostMapping ("/specificDevice")
    public List<SpecificDevice> getSpecificDevice(@RequestBody List<String> list_id){
        return  specificDeviceService.getAllSpecificDevice(list_id);
    }
}
