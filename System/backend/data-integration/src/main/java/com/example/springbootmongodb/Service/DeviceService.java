package com.example.springbootmongodb.Service;


import com.example.springbootmongodb.Repository.DeviceRepository;
import com.example.springbootmongodb.Model.Device;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
@AllArgsConstructor
@Service
public class DeviceService {
    private  final DeviceRepository deviceRepository;

    public List<Device> getAllDevices(){
        return deviceRepository.findAll();
    }
    public List<Device> searchDevice(String name,String color,String ram,String rom){
        return  deviceRepository.findAllByNameContainingIgnoreCaseOrColorIgnoreCaseOrRamIgnoreCaseOrRomIgnoreCase(name,color,ram,rom);
    }
    public Page<Device> findProductsWithPagination(String name,String color,String ram,String rom,int offset,int pageSize){
        Page<Device> devices = deviceRepository.findAllByNameContainingIgnoreCaseOrColorIgnoreCaseOrRamIgnoreCaseOrRomIgnoreCase(name,color,ram,rom,PageRequest.of(offset,pageSize));
        return devices;
    }

}
