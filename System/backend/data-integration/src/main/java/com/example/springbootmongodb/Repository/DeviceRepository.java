package com.example.springbootmongodb.Repository;

import com.example.springbootmongodb.Model.Device;
import com.example.springbootmongodb.Model.SpecificDevice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface DeviceRepository  extends MongoRepository<Device,String> {
    public List<Device> findAllByNameContainingIgnoreCaseOrColorIgnoreCaseOrRamIgnoreCaseOrRomIgnoreCase
            (String name, String color, String ram, String rom);
    public Page<Device> findAllByNameContainingIgnoreCaseOrColorIgnoreCaseOrRamIgnoreCaseOrRomIgnoreCase
    (String name,String color,String ram,String rom, Pageable pageable);


}

